/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.clientes.dao.ClientesJpaController;
import cl.clientes.dao.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.entity.Clientes;

/**
 *
 * @author Andrea
 */
@WebServlet(name = "BaseController", urlPatterns = {"/BaseController"})
public class BaseController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BaseController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BaseController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String accion = request.getParameter("accion");
        
        if (accion.equals("ingreso")) {  
        try {
            String id_cliente = request.getParameter("id_cliente");
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String telefono = request.getParameter("telefono");
            String email = request.getParameter("email");
            
            Clientes clientes = new Clientes();
            clientes.setIdCliente(id_cliente);
            clientes.setRut(rut);
            clientes.setNombre(nombre);
            clientes.setApellido(apellido);
            clientes.setTelefono(telefono);
            clientes.setEmail(email);
            
            ClientesJpaController dao = new ClientesJpaController();
            
            dao.create(clientes);
            
              
           
        } catch (Exception ex) {
            Logger.getLogger(BaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        
         if (accion.equals("verlista")) { 
      
            ClientesJpaController dao = new ClientesJpaController();
            List<Clientes> lista = dao.findClientesEntities();
            request.setAttribute("listaClientes", lista);
            request.getRequestDispatcher("listaClientes.jsp").forward(request, response); 
             
        }
         if (accion.equals("eliminar")) {
        
            try {
                String idcliente = request.getParameter("seleccion");
                ClientesJpaController dao = new ClientesJpaController();
                dao.destroy(idcliente);
                
         
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(BaseController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }  
         if (accion.equals("consultar")) {
                String idcliente = request.getParameter("seleccion");
                ClientesJpaController dao = new ClientesJpaController();
                Clientes cliente = dao.findClientes(idcliente);
                request.setAttribute("cliente", cliente);
                request.getRequestDispatcher("consulta.jsp").forward(request, response); 
            
        } 
         if (accion.equals("editar")) {
            try {
                String id_cliente = request.getParameter("id_cliente");
                String rut = request.getParameter("rut");
                String nombre = request.getParameter("nombre");
                String apellido = request.getParameter("apellido");
                String telefono = request.getParameter("telefono");
                String email = request.getParameter("email");
                
                Clientes clientes = new Clientes();
                clientes.setIdCliente(id_cliente);
                clientes.setRut(rut);
                clientes.setNombre(nombre);
                clientes.setApellido(apellido);
                clientes.setTelefono(telefono);
                clientes.setEmail(email);
                
                ClientesJpaController dao = new ClientesJpaController();
                
                dao.edit(clientes);
                
            } catch (Exception ex) {
                Logger.getLogger(BaseController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }    
          if (accion.equals("consultar")) {
                String idcliente = request.getParameter("seleccion");
                ClientesJpaController dao = new ClientesJpaController();
                Clientes cliente = dao.findClientes(idcliente);
                request.setAttribute("cliente", cliente);
                request.getRequestDispatcher("consulta.jsp").forward(request, response); 
            
        }  
          if (accion.equals("volver")) {
                ClientesJpaController dao = new ClientesJpaController();
            List<Clientes> lista = dao.findClientesEntities();
            request.setAttribute("listaClientes", lista);
            request.getRequestDispatcher("listaClientes.jsp").forward(request, response); 
            
        } 
   
         if (accion.equals("inicio")) {
           
            request.getRequestDispatcher("index.jsp").forward(request, response); 
            
        } 
         processRequest(request, response);
        }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
