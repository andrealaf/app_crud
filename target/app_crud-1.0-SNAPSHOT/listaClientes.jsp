<%-- 
    Document   : listaClientes
    Created on : Apr 24, 2021, 3:42:07 PM
    Author     : Andrea
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.entity.Clientes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Clientes> clientes = (List<Clientes>) request.getAttribute("listaClientes");
    Iterator<Clientes> itClientes = clientes.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista Clientes</title>
    </head>
    <body>
        <h1>Lista de Clientes</h1>
         <form  name="form" action="BaseController" method="POST">

       
        <table border="1">
                    <thead>
                    <th>ID Cliente</th>
                    <th>Rut</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Telefono</th>
                    <th>Email</th>
             
                    <th> </th>
         
                    </thead>
                    <tbody>
                        <%while (itClientes.hasNext()) {
                       Clientes cm = itClientes.next();%>
                        <tr>
                            <td><%= cm.getIdCliente()%></td>
                            <td><%= cm.getRut()%></td>
                            <td><%= cm.getNombre()%></td>
                            <td><%= cm.getApellido()%></td>
                            <td><%= cm.getTelefono()%></td>
                            <td><%= cm.getEmail()%></td>
                         
                 <td> <input type="radio" name="seleccion" value="<%= cm.getIdCliente()%>"> </td>
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                    <br><br>
            <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar Cliente</button>
            <button type="submit" name="accion" value="consultar" class="btn btn-success">Ver Cliente</button>       
            <button type="submit" name="accion" value="inicio" class="btn btn-success">Inicio</button>
        
         </form>   
    </body>
</html>
