<%-- 
    Document   : consulta
    Created on : Apr 24, 2021, 4:58:16 PM
    Author     : Andrea
--%>

<%@page import="root.entity.Clientes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
   Clientes cliente = (Clientes) request.getAttribute("cliente");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Datos Cliente</title>
    </head>
    <body>
        <h1>Datos Cliente</h1>
        <form  name="form" action="BaseController" method="POST">
            
            <label for="id_cliente">ID Cliente : </label>
            <input type="text" name="id_cliente" id="id_cliente" value="<%= cliente.getIdCliente()%>"><br><br>
            
            <label for="rut">RUT : </label>
            <input type="text" name="rut" id="rut" value="<%= cliente.getRut()%>"><br><br>
            
            <label for="nombre">Nombre : </label>
            <input type="text" name="nombre" id="nombre" value="<%= cliente.getNombre()%>"><br><br>
            
            <label for="apellido">Apellido : </label>
            <input type="text" name="apellido" id="apellido" value="<%= cliente.getApellido()%>"><br><br>
            
            <label for="telefono">Telefono : </label>
            <input type="text" name="telefono" id="telefono" value="<%= cliente.getTelefono()%>"><br><br>
            
            <label for="email">Email : </label>
            <input type="text" name="email" id="email" value="<%= cliente.getEmail()%>"><br><br>
            
            <button type="submit" name="accion" value="editar" class="btn btn-success">Editar</button>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>

        </form>
    </body>
</html>
