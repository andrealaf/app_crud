<%-- 
    Document   : index
    Created on : Apr 20, 2021, 11:59:49 AM
    Author     : Andrea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Clientes BD</title>
    </head>
    <body>
        <h1>Consultar Base de Datos Clientes</h1>
       
        <form  name="form" action="BaseController" method="POST">
            
            <label for="id_cliente">ID Cliente : </label>
            <input type="text" name="id_cliente" id="id_cliente"><br><br>
            
            <label for="rut">RUT : </label>
            <input type="text" name="rut" id="rut"><br><br>
            
            <label for="nombre">Nombre : </label>
            <input type="text" name="nombre" id="nombre"><br><br>
            
            <label for="apellido">Apellido : </label>
            <input type="text" name="apellido" id="apellido"><br><br>
            
            <label for="telefono">Telefono : </label>
            <input type="text" name="telefono" id="telefono"><br><br>
            
            <label for="email">Email : </label>
            <input type="text" name="email" id="email"><br><br>
            
            <button type="submit" name="accion" value="verlista" class="btn btn-success">Ver Lista Clientes</button>
            <button type="submit" name="accion" value="ingreso" class="btn btn-success">Crear Cliente</button>

        </form>
    </body>
</html>
